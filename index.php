<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/notiny.min.css">
        <link rel="stylesheet" href="css/login.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
          <?php
            require_once './php/model/LoginModel.php';
           
            
           if(isset($_POST))
           {
               if(filter_has_var(INPUT_POST, "usuario") && filter_has_var(INPUT_POST, "password"))
               {
                  
                    $usuario=filter_input(INPUT_POST, "usuario");
                    $password=  filter_input(INPUT_POST, "password");
                    if(empty($usuario)){
                        $error="¡Ingresa tu nombre de usuario";
                    }
                    else if(empty ($password)){
                        $error="¡Ingresa tu contraseña";
                    }
                    else{
                        $login = new LoginModel();
                        if($login->iniciarLogin($usuario, $password)){
                           $login->redirect("menu.php");
                        }
                        else{
                            $error="¡Tu usuario o password son incorrectos";
                        }
                    }
                   
               }
               
              
           } ?>
          <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                  <a class="navbar-brand" href="#"><span><img src="img/logo-siaisa.png" width="35" height="35"/></span>SIAISA</a>
              </div>
       
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hid"><a href="#">Recuerda que tu usuario y contraseña son sólo para tu uso ¡NO lo prestes!</a></li>
                </ul>
              </div>
            </div>
   </nav>
        <div class="container">
        <div class="row">
			<div class="col-md-5 col-md-offset-3  col-sm-6 col-sm-offset-3 ">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">
								<a href="#" class="azul-font" >INICIAR SESIÓN</a>
							</div>
						</div>
						<hr>
					</div>
                                      
					<div class="panel-body">
						<div class="row">
                                                    
							<div class="col-lg-12">
                                                                             
                                                              <?php
                                                                   if(isset($error))
                                                                  {
                                                                        ?>
                                                                        <div class="alert alert-danger">
                                                                            <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                                                                        </div>
                                                                        <?php
                                                                   }
                                                               ?>
                                                            <form id="login-form" action="index.php" method="post" role="form" style="display: block;">
									<div class="form-group">
                                                                            <input type="text" name="usuario" id="username" tabindex="1" class="form-control form-control-shadow" placeholder="Usuario" required>
									</div>
									<div class="form-group">
                                                                            <input type="password" name="password" id="password" tabindex="2" class="form-control form-control-shadow" placeholder="Contraseña" required>
									</div>
					                               
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-5">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-red" value="Entrar">
											</div>
                                                                                     
                                                                                    
										</div>
									</div>
						                   
								</form>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        
        <footer>
            <span>2016 SIAISA</span>
        </footer>
       

    </body>
</html>
