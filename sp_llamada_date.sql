CREATE DEFINER=`evnsmx_sisa`@`%.%.%.%` PROCEDURE `sp_encuesta_llamadas_filtro_agente_by_date`(in id_agent int(11),in fecha varchar(20))
BEGIN
DECLARE _contactados int(11);
DECLARE _no_contactados int(11);
DECLARE _total_contactados int(11);

set _contactados = (select  count(detalles_contacto_encuesta.id_contacto)
from bitacora_encuesta inner join detalles_contacto_encuesta on detalles_contacto_encuesta.llamada_id =  bitacora_encuesta.llamada_id where bitacora_encuesta.id_agente= id_agent and detalles_contacto_encuesta.id_contacto = 1 and DATE(bitacora_encuesta.inicio)= STR_TO_DATE(fecha,  '%Y-%m-%d'));
set  _no_contactados = (select  count(detalles_contacto_encuesta.id_contacto)
from bitacora_encuesta inner join detalles_contacto_encuesta on detalles_contacto_encuesta.llamada_id =  bitacora_encuesta.llamada_id where bitacora_encuesta.id_agente= id_agent and detalles_contacto_encuesta.id_contacto = 0 and DATE(bitacora_encuesta.inicio)= STR_TO_DATE(fecha,  '%Y-%m-%d'));
set _total_contactados= (select count(llamada_id) from bitacora_encuesta where llamada_id <> 0 and id_agente = id_agent and DATE(bitacora_encuesta.inicio)= STR_TO_DATE(fecha,  '%Y-%m-%d'));
select agente.nombre , _contactados
	,_no_contactados    ,
	 _total_contactados
    from agente 
    where agente.id_agente= id_agent;



END