CREATE DEFINER=`evnsmx_sisa`@`%.%.%.%` PROCEDURE `sp_todito`(					 in id_agent int,
												 in id_client_encuesta int,
                                                 in inicio datetime,
                                                 in fin datetime,
                                                 in tipo_llamada int,
                                                 in notas varchar(150),
                                                 in status_llamada int,
                                                 in id_contacto int,
                                                 in interlocutor_nombre varchar(45),
                                                 in interlocutor_paterno varchar(45),
                                                 in interlocutor_materno varchar(45),
                                                 in id_motivo_no_contacto int,
                                                 in id_enterado int,
                                                 in tipo_interesaria int,
                                                 in agendar_cita datetime,
                                                 in id_medio_entero int,
                                                in tipo_objecion int,
                                                in plazo numeric,
                                                in monto numeric, 
                                                in Sucursal varchar(45),
                                                in Ejecutivo varchar(45),
                                                in problematica varchar(45),
                                                in pago_mensual numeric,
                                                in nombre_prospecto varchar(45),
                                                in telefono_prospecto numeric,
                                                in email_prospecto varchar(45),                                                
                                                in calificacion int,
                                                in recomendaria_producto int,
                                                in credito_otra_financiera int,
                                                in tomaria_otra_financiera int,
                                                in correo_electronico varchar(45),
                                                in entidad_financiera varchar(45))
BEGIN                                        
DECLARE _id_llamada int;
DECLARE _id_interesariaCredito int;
DECLARE _id_prioridad int;
set _id_llamada = (SELECT max(llamada_id) FROM bitacora_encuesta) + 1 ;
set _id_interesariaCredito = (SELECT max(id_interesariaCredito) FROM interesariaCredito_encuesta) +1;
if _id_llamada = 0 or (_id_llamada) is null then
set _id_llamada =1;
end if;
if _id_interesariaCredito = 0 or (_id_interesariaCredito) is null then
set _id_interesariaCredito =1;
end if;

   INSERT INTO  `bitacora_encuesta` (`inicio`, `fin`, `tipo_llamada`, `notas`, `id_cliente`, `id_agente`, `id_status_llamada`)  VALUES(inicio, fin, tipo_llamada, notas, id_client_encuesta, id_agent, status_llamada);
    INSERT INTO `detalles_contacto_encuesta` ( `interlocutor_nombre`, `interlocutor_paterno`, `interlocutor_materno`, `id_contacto`, `llamada_id`, `id_motivo_no_contacto`) VALUES ( interlocutor_nombre, interlocutor_paterno, interlocutor_materno, id_contacto, _id_llamada, id_motivo_no_contacto);
    INSERT INTO `interesariaCredito_encuesta` (`id_interesariaCredito`,`id_enterado`,`tipo_interesaria`, `agendar_cita`,  `id_medio_entero`, `llamada_id`) VALUES (_id_interesariaCredito,id_enterado, tipo_interesaria, agendar_cita, id_medio_entero , _id_llamada);
	INSERT INTO `manejoObjecion_encuesta` (`tipo_objecion`, `plazo`, `monto`, `Sucursal`, `Ejecutivo`, `problematica`, `pago_mensual`, `id_interesariaCredito`) VALUES (tipo_objecion, plazo,monto, Sucursal, Ejecutivo, problematica, pago_mensual, _id_interesariaCredito);
    INSERT INTO `prospectos_encuesta` (`nombre_prospecto`, `telefono_prospecto`, `email_prospecto`, `id_interesariaCredito`) VALUES (nombre_prospecto, telefono_prospecto, email_prospecto, _id_interesariaCredito);
    INSERT INTO `evnsmx_cobra`.`encuesta_cierre` (`calificacion`, `recomendaria_producto`, `credito_otra_financiera`, `tomaria_otra_financiera`, `correo electronico`, `entidad_financiera`, `id_interesariaCredito`) VALUES (calificacion, recomendaria_producto, credito_otra_financiera, tomaria_otra_financiera, correo_electronico, entidad_financiera, _id_interesariaCredito);
    
   if id_motivo_no_contacto = 3 or id_motivo_no_contacto = 4 or id_motivo_no_contacto = 5 then
		UPDATE `evnsmx_cobra`.`cliente_encuesta` SET `id_status_visible`='0' , `fin_ultimo_intento`=fin,`en_linea`='0',`en_linea_con`='0' WHERE `id_cliente`=id_client_encuesta;
    else
		UPDATE `evnsmx_cobra`.`cliente_encuesta` SET `fin_ultimo_intento`=fin,`en_linea`='0',`en_linea_con`='0' WHERE `id_cliente`=id_client_encuesta;
    end if;
  
END